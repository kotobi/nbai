from database import Database
import datetime
import pandas as pd
import numpy as np
import os
import glob

class LoadDb(object):
    def __init__(self):
        self.db = Database()

    def _get_stats_type(self, filename):
        if 'player_stats' in filename:
            return 'player_stats'
        elif 'team_stats' in filename:
            return 'team_stats'
        elif 'player_info' in filename:
            return 'player_info'
        else:
            print('Unrecognized csv file')
            return None

    def _read_file(self, filename):
        data = pd.read_csv(filename)
        data = data.replace({np.nan: None})
        if 'date' in data.columns:
            data['date'] = data['date'].apply(lambda x: datetime.datetime.fromisoformat(x))
        if 'Birth Date' in data.columns:
            data['Birth Date'] = data['Birth Date'].apply(lambda x: datetime.datetime.strptime(x, '%B %d, %Y') if x is not None else None)
            data['From'] = data['From'].astype(int)
            data['To'] = data['To'].astype(int)

        data = data.to_dict(orient='records')
        data = [{k.lower().replace(' ', '_'): v for k, v in d.items()} for d in data]
        return data

    def load_database_from_dir(self, filepath):
        filepath = os.path.join('.', 'data', '*.csv')
        files = glob.glob(filepath)
        num_files = len(files)
        files.sort()
        for i, f in enumerate(files):
            if i % 100 == 0:
                print('Loading {}/{}'.format(i, num_files))
            self.load_database_from_file(f)

    def load_database_from_file(self, filename):
        stats_type = self._get_stats_type(filename)
        data = self._read_file(filename)
        if stats_type is None:
            return
        elif stats_type == 'player_stats':
            self.db.insert_player_stat(data)
        elif stats_type == 'team_stats':
            self.db.insert_team_stat(data)
        elif stats_type == 'player_info':
            self.db.insert_player_info(data)

if __name__ == '__main__':
    filepath = '/home/xjuner/Documents/ai/nba/data'
    load_db = LoadDb()
    load_db.load_database_from_dir(filepath)

