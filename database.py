import pymongo
PLAYER_STAT = 'player_stat'
TEAM_STAT = 'team_stat'
  
  
class Database(object):
    def __init__(self):
        self.myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        self.mydb = self.myclient["nba"]
        self.player_stats_col = self.mydb["player_stats"]
        self.player_stats_col.create_index([('player', 1), ('team', 1), ('date', 1)], unique=True)
        self.team_stats_col = self.mydb["team_stats"]
        self.team_stats_col.create_index([('team', 1), ('date', 1)], unique=True)
        self.player_info_col = self.mydb["player_info"]
        self.player_info_col.create_index([('player', 1), ('birth_date', 1)], unique=True)

    def insert_player_stat(self, data):
        try:
            self.player_stats_col.insert_many(data, ordered=False)
        except:
            for d in data:
                try:
                    self.player_stats_col.insert(d)
                except:
                    continue


    def insert_team_stat(self, data):
        try:
            self.team_stats_col.insert_many(data, ordered=False)
        except:
            for d in data:
                try:
                    self.team_stats_col.insert(d)
                except:
                    continue
        
    def insert_player_info(self, data):
        try:
            self.player_info_col.insert_many(data, ordered=False)
        except:
            for d in data:
                try:
                    self.player_info_col.insert(d)
                except:
                    continue


if __name__ == '__main__':
    data = {
        'Player': 'Gary Payton',
        'MP': '0:03',
        'FG': 0.0,
        'FGA': 0.0,
        'FG%': None,
        '3P': 0.0,
        '3PA': 0.0,
        '3P%': None,
        'FT': 0.0,
        'FTA': 0.0,
        'FT%': None,
        'ORB': 0.0,
        'DRB': 0.0,
        'TRB': 0.0,
        'AST': 0.0,
        'STL': 0.0,
        'BLK': 0.0,
        'TOV': 0.0,
        'PF': 0.0,
        'PTS': 0.0,
        '+/-': 0.0,
        'team': 'GSW',
        'location': 'Chase Center, San Francisco, California',
        'date': '2021-10-21 22:00:00'
    }
    data = {k.lower(): v for k, v in data.items()}
    print(data.keys())
    db = Database()
    # filename = 'scrape_nba/scrape_nba/spiders/data/2021-10-21 22:00:00_LAC_GSW_player_stats.csv'
    # df = pd.read_csv(filename)
    # db.insert_player_stat(data)
    
  
