#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 20 17:00:56 2021

@author: xjuner
"""

import numpy as np
import matplotlib.pyplot as plt

win_loss_output = {True: 'Win', False: 'Loss'}

PLOT_RESULT = True

accuracy = 0.56
games_per_round = 32
num_rounds = 25
num_sims = 100

juice_factor = 0.9
# juice_factor = 1

bet_fraction = 0.02
max_bet_fraction = 1 / games_per_round
if bet_fraction > max_bet_fraction:
    bet_fraction = max_bet_fraction
print('Bet percentage: {:.2f}%'.format(bet_fraction * 100))
final_purse = []
num_bankrupts = 0

for sim_count in range(num_sims):
    purse_history_for_round = []
    purse = 1
    for this_round in range(num_rounds):
        bet_size_per_game = purse * bet_fraction
        results = accuracy >= np.random.rand(games_per_round)
        num_wins = sum(results)
        num_losses = games_per_round - num_wins
        # purse -= bet_size_this_round
        round_earnings = bet_size_per_game * (juice_factor * num_wins - num_losses)
        purse += round_earnings
        # print('{}! num_wins: {}, round_earnings: {:.3f}, purse: {:.3f}'.format(win_loss_output[num_wins > num_losses], num_wins, round_earnings, purse))
        # print(purse)
        if purse <= 0:
            purse = 0
            # print('bankrupt')
            num_bankrupts += 1
            continue # bankrupt
    final_purse.append(purse)

# print('purse: {:.3f}'.format(purse))
final_purse = np.array(final_purse)
print('Profit {}% of simulations. Lost all money {}%'.format(100 * sum(final_purse > 1) / len(final_purse),
                                                             num_bankrupts/len(final_purse)))
print('Average return: {:.2f}%\nMax return: {:.2f}%\nMin return: {:.2f}%'.format(100 * (final_purse - 1).mean(), 100 * (final_purse - 1).max(), 100 * (final_purse - 1).min()))

if PLOT_RESULT:
    f = plt.figure()
    ax = f.add_subplot(111)
    ax.plot(final_purse)
    ax.plot([0, num_sims], [1, 1])
    ax.set_ylim([0, 3])
    f.savefig('sim.png')
