import scrapy                                                                                                                                                                                          
import pandas as pd                                                                                 
import dateutil.parser as dup                                                                       
                                                                                                    
                                                                                                    
class GetstatsSpider(scrapy.Spider):                                                                
    name = 'game_stats'                                                                               
    allowed_domains = ['https://www.basketball-reference.com']                                      
    start_urls = ['https://www.basketball-reference.com/boxscores/202012220BRK.html']               
#    start_urls = ['https://www.basketball-reference.com/leagues/NBA_2021_games.html']              
    # start here ^^^ and "click" on the different months                                            
                                                                                                    
    def _get_game_basic_tables(self, tables):                                                       
        tables =  [(t.attrib['id'].split('-')[1], t) for t in tables if 'game-basic' in t.attrib['id']]
        return tables  # return list of tuples (team_abbreviation, table html)                      
                                                                                                    
    def _filter_columns(self, columns):                                                             
        return ['Player'] + columns[1:]                                                             
                                                                                                    
    def parse(self, response):                                                                      
        print('\n\n\n')                                                                             
        print('='*20)                                                                               
        print('Parsing')                                                                            
        print('='*5)                                                                                
        tables = response.xpath('//table')                                                          
        tables = self._get_game_basic_tables(tables)                                                
                                                                                                    
        # get date and location                                                                     
        scorebox = response.xpath('//div[@class="scorebox_meta"]')                                  
        date_time, location, _, _, _, _ = scorebox.xpath('div//text()').getall()                    
        date_time_object = dup.parse(date_time)                                                     
                                                                                                    
        # get player stats                                                                          
        for i, (team, table) in enumerate(tables):                                                  
            print('*'*10)                                                                           
            header = table.xpath('thead//tr')                                                       
            print(header[0])                                                                        
            column_headers = self._filter_columns(header[1].xpath('th//text()').getall() + ['team'] + ['location'])
            if i == 0:  # initialize only once                                                      
                player_stats_df = pd.DataFrame(columns=column_headers)                              
                team_stats_df = pd.DataFrame(columns=column_headers)                                
            print('Column headers = ', column_headers)                                              
#            if header[0].xpath('th//text()').get() == 'Basic Box Score Stats':                     
            body = table.xpath('tbody')                                                             
            rows = body.xpath('tr')                                                                 
            for row in rows:                                                                        
                player_name = row.xpath('th//text()').get()                                         
                if player_name == 'Reserves':                                                       
                    print('===== Reserves =======')                                                 
                    continue                                                                        
                row_data = [player_name] + [r.xpath('text()').get() for r in row.xpath('td')] + [team, location]
                row_dict = {k:v for k,v in zip(column_headers, row_data)}                           
                player_stats_df = player_stats_df.append(row_dict, ignore_index=True)               
                print(row_data)                                                                     
                                                                                                    
            # get team stats                                                                        
            if table.xpath('tfoot//th//text()').get() == 'Team Totals':                             
                team_stats = [''] + table.xpath('tfoot//td//text()').getall() + [None, team, location]
                row_dict = {k:v for k,v in zip(column_headers, team_stats)}                         
                team_stats_df = team_stats_df.append(row_dict, ignore_index=True)                   
                                                                                                    
                                                                                                    
        filename_starter = str(date_time_object) + '_' + '_'.join([t[0] for t in tables])  # ugly, fix
        player_stats_df.to_csv(filename_starter + '_' + 'player_stats.csv', index=False)            
        team_stats_df.to_csv(filename_starter + '_' + 'team_stats.csv', index=False)                
        print('='*20)                                                                               
                                                                                                    


