import scrapy                                                                                                                                                                                          
import pandas as pd                                                                                 
import dateutil.parser as dup               
import datetime
import urllib
                                                                                                    
                                                                                                    
class GetstatsSpider(scrapy.Spider):                                                                
    name = 'players'                                                                               
    allowed_domains = ['https://www.basketball-reference.com']                                      
    url = 'https://www.basketball-reference.com/players/'
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    custom_settings = {
        'ROBOTSTXT_OBEY': True,
        'DOWNLOAD_DELAY': 2,
    }    
    start_urls = []
#    game_date = datetime.datetime.today()
    game_date = datetime.datetime(2017, 11, 25)
    for letter in alphabet:
        start_urls.append(url + letter + '/')
        print(start_urls[-1])
                                                                                                    
    def _get_game_basic_tables(self, tables):                                                       
        tables =  [(t.attrib['id'].split('-')[1], t) for t in tables if 'game-basic' in t.attrib['id']]
        return tables  # return list of tuples (team_abbreviation, table html)                      
                                                                                                    
    def _filter_columns(self, columns):                                                             
        return ['Player'] + columns[1:]                                                             
                                                                                                    
    def parse(self, response):
        
        print('=*-/'*20)
        print(response.url)
        letter = response.url.split('/')[-2:]
        if letter[-1] == '':
            letter = letter[-2]
        else:
            letter = letter[-1]
        games = response.xpath(".//div[contains(@class, 'game_summary')]")
        players_table = response.xpath('.//table[@id="players"]')[0]
        players = players_table.xpath('./tbody/tr')
        headers = players_table.xpath('./thead//th//text()').getall()
        df = pd.DataFrame([], columns=headers)
        for p in players:
            player_name = p.xpath('.//th//a/text()').get()
            player_data = []
            for ptd in p.xpath('.//td'):
                player_data.append(ptd.xpath('.//text()').get(default=''))
#            player_data = p.xpath('.//td//text()').getall()
            player_row = {k:v for k, v in zip(headers, [player_name] + player_data)}
            df = df.append(player_row, ignore_index=True)
        df.to_csv('player_info_' + letter + '.csv', index=False)
        print(df)



       
                                                                                                    


