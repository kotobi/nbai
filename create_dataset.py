#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 24 19:26:25 2021

@author: xjuner
"""

from database import Database
import datetime
db = Database()

query = [
    # {'$match': {'player': 'LeBron James'}}, 
    {'$match': {'pts': {"$ne" :None}}},
    {'$group': {
        '_id': '$player',
        'avg_pts': {'$avg': '$pts'},
        'games_played': {"$sum": 1},
        }
    },
    {'$sort': {'avg_pts': -1}},

    ]

query = [
    # {'$match': {'player': 'LeBron James'}}, 
    {'$match': {'date': {"$gt" : datetime.datetime(2019, 1, 1)}}},
    {'$sort': {'date': 1}},

    {'$group': {
        '_id': '$player',
        'records': {'$addToSet': '$team'},
        'games_played': {"$sum": 1},
        }
    },
    ]

# query = [
#     # {'$match': {'player': 'LeBron James'}}, 
#     {'$match': {'date': {"$gt" : datetime.datetime(2019, 1, 1)}}},
#     {'$project': {'_id': 1, 'player': 1, 'date': 1}},
#     {'$sort': {'date': 1}}
#     ]


x = list(db.player_stats_col.aggregate(query, allowDiskUse=True))
# print(x)


